from flask import Flask, jsonify, request, Response
from settings import *
from studentModel import *

# / GET STUDENTS
@app.route('/students')
def get_students():
    return(jsonify({'students': Student.get_all_students()}))

#POST / Add a student
@app.route('/students', methods=['POST'])
def add_student():
    request_data = request.get_json()
    # return (jsonify(request_data))
    Student.add_student(request_data['name'], request_data['classid'], request_data['studentid'])
    # return(jsonify({'students': Student.get_all_students()}))
    response = Response("", 201, mimetype='application/json')
    return response

app.run(port=5000)