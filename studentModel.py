from flask import Flask
from flask_sqlalchemy import SQLAlchemy
import json
from settings import app

db = SQLAlchemy(app)

class Student(db.Model):
    __tablename__ = 'students'
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(40), nullable=False)
    classid = db.Column(db.Integer, nullable=False)
    studentid = db.Column(db.Integer, nullable=False)

    def json(self):
        return {'name' : self.name, 'classid' : self.classid, 'studentid' : self.studentid}

    def add_student(_name, _classid, _studentid):
        new_student = Student(name=_name, classid=_classid, studentid=_studentid)
        db.session.add(new_student)
        db.session.commit()

    def get_all_students():
        return [Student.json(student) for student in Student.query.all()]

    # def __repr__(self):
    #     student_details = {
    #         'name' : self.name,
    #         'classid' : self.classid,
    #         'studentid' : self.studentid
    #     }
    #     return json.dumps(student_details)